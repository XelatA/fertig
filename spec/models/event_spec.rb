require 	'rails_helper'

describe Event do
  before do
    @event = Event.new(name: "Hochzeit", eventtext: "Deko:weiß/silber, DJ, Orientalisches Catering", guest: "120", deadline: "12.05.2017")
  end

  subject { @event }

  it {should respond_to(:name) }
  it {should respond_to(:eventtext) }
  it {should respond_to(:guest) }
  it {should respond_to(:deadline) }

  it {should be_valid }

  describe "when name is not present" do
    before {@event.name = "" }
    it {should_not be_valid }
  end

  describe "when eventtext is not present" do
    before {@event.eventtext = "" }
    it {should_not be_valid }
  end

  describe "when guest is not present" do
    before {@event.guest = "" }
    it {should_not be_valid }
  end

  describe "when deadline is not present" do
    before {@event.deadline = "" }
    it {should_not be_valid }
  end
end
